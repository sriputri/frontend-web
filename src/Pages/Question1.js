import {useEffect, useState} from "react";
import {Table, Tag, Space, Input} from 'antd';
import search from "../Asset/search.svg";

const { Search } = Input;

const dataDummy = [
    {
        key: '1',
        name: 'Incredible Fresh Bike',
        codeProd: 10,
        category:"1",
        description: 'Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',
        tags: ["Industrial",
            "Garden",
            "Music",
            "Movies",
            "Grocery"],
    },
    {
        key: '2',
        name: 'Practical Fresh Salad',
        codeProd: 11,
        category:"2",
        description: 'The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',
        tags: ["Clothing",
            "Tools",
            "Electronics",
            "Outdoors",
            "Computers"],
    },
    {
        key: '3',
        name: 'Intelligent Granite Table',
        codeProd: 12,
        category:"3",
        description: 'The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',
        tags: ["Computers",
            "Jewelery",
            "Books",
            "Music",
            "Automotive"],
    },
    {
        key: '4',
        name: 'Tasty Concrete Chair',
        codeProd: 13,
        category:"4",
        description: 'The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',
        tags: ["Games",
            "Electronics"],
    },
    {
        key: '5',
        name: 'Gorgeous Soft Mouse',
        codeProd: 14,
        category:"5",
        description: 'The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality',
        tags: ['nice', 'developer'],
    },
    {
        key: '6',
        name: 'Sleek Cotton Ball',
        codeProd: 15,
        category:"6",
        description: 'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
        tags: ['nice', 'developer',"Movies",
            "Industrial",
            "Home",
            "Automotive",
            "Outdoors"],
    },
    {
        key: '7',
        name: 'Awesome Fresh Tuna',
        codeProd: 16,
        category:"7",
        description: 'New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',
        tags: ['nice', 'Food'],
    },
    {
        key: '8',
        name: 'Incredible Steel Bacon',
        codeProd: 17,
        category:"8",
        description: 'The Football Is Good For Training And Recreational Purposes',
        tags: ["Shoes",
            "Toys",
            "Games",
            "Tools",
            "Electronics"],
    },
    {
        key: '9',
        name: 'Fantastic Plastic Computer',
        codeProd: 18,
        category:"9",
        description: 'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
        tags: ['nice', 'developer'],
    },
];

function Question1() {

    const [state, setState] = useState({})
    const [dataShow, setDataShow] = useState([])
    // const [dataCat, setDataCat] = useState([])
    // const [order, setOrder] = useState("ascend")

    let {sortedInfo, filteredInfo } = state;
    sortedInfo = sortedInfo || {};
    filteredInfo =filteredInfo ||{};

    const columns = [
        {
            title: 'Product Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => a.name.localeCompare(b.name),
            sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
            ellipsis: true,
            sortDirections: ['ascend'],
        },
        {
            title: 'Product Code',
            dataIndex: 'codeProd',
            key: 'codeProd',
        },
        {
            title: 'Category',
            dataIndex: 'category',
            key: 'category',
        },
        {
            title: 'Description',
            key: 'description',
            dataIndex: 'description',
        },
        {
            title: 'Tags',
            key: 'tags',
            dataIndex: 'tags',
            render: tags => (
                <>
                    {tags.map(tag => {
                        return (
                            <ul>
                                <li>
                                    {tag}
                                </li>
                            </ul>
                        );
                    })}
                </>
            ),
        },
    ];


    useEffect(() => {
        setDataShow(dataDummy)
    }, [])

    // =======API========
    // useEffect(() => {
    //     fetch('https://screening.moduit.id/frontend/web/question/one')
    //         .then((response) => response.json())
    //         .then((json) => setDataCat(json))
    // }, [])

    function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
        setState({
            filteredInfo: filters,
            sortedInfo: sorter
        })
    }


    return (
        <div>
            <h2>Question 1</h2>
            {/*=================  SEARCH =======================*/}
            {/*<div style={{ padding:"0 0 10px 770px"}}>*/}
            {/*    <Input placeholder="Search by product name"  onChange={(value) => {if(value.target.value == "" || value.target.value == null){*/}
            {/*        console.log("mas")*/}
            {/*        setDataShow(dataDummy)*/}
            {/*    }else {*/}
            {/*        console.log("kel")*/}
            {/*        console.log(value.target.value,"value")*/}
            {/*        setDataShow(dataDummy.filter(d=> d.name == value.target.value))*/}
            {/*    }}}*/}
            {/*    style={{width: 250, borderRadius:10}} icon={<img src={search} />}/>*/}
            {/*</div>*/}
        <div style={{border:"0", borderRadius:"50px"}}>
            <Table columns={columns} dataSource={dataShow} onChange={onChange} />
        </div>
        </div>

    );
}

export default Question1;
