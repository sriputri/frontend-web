import {useEffect, useState} from "react";
import { Table, Tag, Space } from 'antd';
import { AudioOutlined, SearchOutlined } from '@ant-design/icons';
import { Input } from 'antd';
import q1 from "../Asset/Q1.svg";

const { Search } = Input;



const data = [
    {
        key: '1',
        name: 'Incredible Fresh Bike',
        codeProd: 10,
        category:"1",
        description: 'Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',
        tags: ["Industrial",
            "Garden",
            "Music",
            "Movies",
            "Grocery"],
    },
    {
        key: '2',
        name: 'Practical Fresh Salad',
        codeProd: 11,
        category:"2",
        description: 'The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',
        tags: ["Clothing",
            "Tools",
            "Electronics",
            "Outdoors",
            "Computers"],
    },
    {
        key: '3',
        name: 'Intelligent Granite Table',
        codeProd: 12,
        category:"3",
        description: 'The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',
        tags: ["Computers",
            "Jewelery",
            "Books",
            "Music",
            "Automotive"],
    },
    {
        key: '4',
        name: 'Tasty Concrete Chair',
        codeProd: 13,
        category:"4",
        description: 'The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',
        tags: ["Games",
            "Electronics"],
    },
    {
        key: '5',
        name: 'Gorgeous Soft Mouse',
        codeProd: 14,
        category:"5",
        description: 'The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality',
        tags: ['nice', 'developer'],
    },
    {
        key: '6',
        name: 'Sleek Cotton Ball',
        codeProd: 15,
        category:"6",
        description: 'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
        tags: ['nice', 'developer',"Movies",
            "Industrial",
            "Home",
            "Automotive",
            "Outdoors"],
    },
    {
        key: '7',
        name: 'Awesome Fresh Tuna',
        codeProd: 16,
        category:"7",
        description: 'New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',
        tags: ['nice', 'Food'],
    },
    {
        key: '8',
        name: 'Incredible Steel Bacon',
        codeProd: 17,
        category:"8",
        description: 'The Football Is Good For Training And Recreational Purposes',
        tags: ["Shoes",
            "Toys",
            "Games",
            "Tools",
            "Electronics"],
    },
    {
        key: '9',
        name: 'Fantastic Plastic Computer',
        codeProd: 18,
        category:"9",
        description: 'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',
        tags: ['nice', 'developer'],
    },
    {
        key: '10',
        name: ' Fresh Tuna',
        codeProd: 19,
        category:"10",
        description: 'New range of formal shirts are designed keeping you in mind.',
        tags: ['nice', 'Food'],
    },
    {
        key: '11',
        name: ' Steel Bacon',
        codeProd: 20,
        category:"11",
        description: 'The Football Is Good For Training',
        tags: ["Shoes",
            "Toys",
            "Games",
            "Tools",
            "Electronics"],
    },
    {
        key: '12',
        name: ' Computer',
        codeProd: 21,
        category:"12",
        description: 'New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U 2016',
        tags: ['nice', 'developer'],
    },
];


function Question1() {

    const [dataQ1, setDataQ1] = useState([])
    const [dataAll, setDataAll] = useState([])
    const [state, setState] = useState({})

    let {sortedInfo, filteredInfo } = state;
    sortedInfo = sortedInfo || {};
    filteredInfo =filteredInfo ||{};

    const columns = [
        {
            title: 'Product Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => a.name.localeCompare(b.name),
            sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
            ellipsis: true,
            sortDirections: ['ascend'],
        },
        {
            title: 'Product Code',
            dataIndex: 'codeProd',
            key: 'codeProd',
        },
        {
            title: 'Category',
            dataIndex: 'category',
            key: 'category',
        },
        {
            title: 'Description',
            key: 'description',
            dataIndex: 'description',
        },
        {
            title: 'Tags',
            key: 'tags',
            dataIndex: 'tags',
            render: tags => (
                <>
                    {tags.map(tag => {
                        return (
                            <ul>
                                <li>
                                    {tag}
                                </li>
                            </ul>
                        );
                    })}
                </>
            ),
        },
    ];

    // ======SEARCH NAME PAKAI DATA API=========
    // const handleFilter = (value) => {
    //     if(value.target.value == "" || value.target.value == null){
    //         setDataQ1(dataAll)
    //     }else {
    //         setDataQ1(dataAll.filter(d=> d.name == value.target.value))
    //
    //     }
    // }

    function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
        setState({
            filteredInfo: filters,
            sortedInfo:sorter
        })
    }

    useEffect(() => {
        setDataAll(data)
    }, [])


    // ==========API===========
    // useEffect(() => {
    //     fetch('https://screening.moduit.id/frontend/web/question/one')
    //         .then((response) => response.json())
    //         .then((json) => {
    //             setDataQ1(json.children_data)
    //             setDataAll(json.children_data)
    //         })
    // }, [])
    return (
        <div>
            <h2>Question 2</h2>
            <div style={{ padding:"0 0 10px 770px"}}>
                <Input placeholder="Search by product name"  onChange={(value) => {if(value.target.value == "" || value.target.value == null){
                    setDataAll(data)
                }else {
                    setDataAll(data.filter(d=> d.name == value.target.value))
                }}} style={{width: 250, borderRadius:10}} suffix={<SearchOutlined />}/>
                {/*<Search placeholder="Search by product name" onSearch={handleFilter} style={{ width: 250,  borderRadius:10 }} />*/}
            </div>
        <div>
            {console.log((dataQ1), " tada")}
            <Table columns={columns} dataSource={dataAll} onChange={onChange} style={{border:0 , borderRadius:"50px"}} />
        </div>
        </div>
    );
}

export default Question1;
