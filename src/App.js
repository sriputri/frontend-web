import './App.css';
import logo from './Asset/logo.svg';
import q1 from './Asset/Q1.svg'
import q2 from './Asset/Q2.svg'
import {BrowserRouter, BrowserRouter as Router, Link, Route} from "react-router-dom";
import IndexRouter from "./routes";
import { Layout, Menu } from 'antd';

const { Header, Content, Footer, Sider } = Layout;

function App() {
  return (
      <BrowserRouter>
      <Layout>
          <Sider width={250}>
              <Menu
                  mode="inline"
                  defaultSelectedKeys={['1']}
                  defaultOpenKeys={['sub1']}
                  style={{ height: '100%', borderRight: 0 }}
              >
                  <div style={{
                      display: "flex",
                      justifyContent: "center",
                      alignSelf: "center",
                      padding:"30px 0 30px 0" }}>
                      <img src={logo} />
                  </div>
                      <Menu.Item key="1" icon={<img src={q1} />}>
                          <Link to={"/question1"} >Question 1</Link>
                      </Menu.Item>
                      <Menu.Item key="2" icon={<img src={q2} />}>
                          <Link to={"/question2"} >Question 2</Link>
                      </Menu.Item>
              </Menu>
          </Sider>
          <Layout>
              <Content style={{ margin: '24px 16px 0' }}>
                  <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                      <section>
                          <IndexRouter/>
                      </section>
                  </div>
              </Content>
              <Footer style={{ textAlign: 'center' }}>Copyright © 2019 PT Moduit Digital Indonesia. All rights reserved</Footer>
          </Layout>
      </Layout>
      </BrowserRouter>
  );
}

export default App;
