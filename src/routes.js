import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import Question1 from "./Pages/Question1";
import Question2 from "./Pages/Question2";

const IndexRouter = () => (
    // <Router>
        <Routes>
        <Route path="/" exact element={<Question1/>}/>
        <Route path="/question1"  element={<Question1/>}/>
        <Route path="/question2" element={<Question2/>}/>
        </Routes>
    // </Router>
)

export default IndexRouter;